package org.jim.server.helper.db;

import java.util.Arrays;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
import org.jim.common.ImConfig;
import org.jim.common.message.DBUtilsHelper;

import com.alibaba.druid.pool.DruidDataSource;

public class DBUtils implements DBUtilsHelper{

	protected ImConfig imConfig;
	protected DruidDataSource dataSource;
	// private Logger log = LoggerFactory.getLogger(DBUtils.class);

	public DBUtils(ImConfig imConfig){
		this.imConfig = imConfig;
		this.dataSource = new DruidDataSource();
		this.dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		this.dataSource.setUrl(imConfig.getSqlUrl());
		this.dataSource.setUsername(imConfig.getSqlUser());
		this.dataSource.setPassword(imConfig.getSqlPassword());
		this.dataSource.setConnectionInitSqls(Arrays.asList("set names utf8mb4;"));
		this.dataSource.setValidationQuery("select 1");
		this.dataSource.setTestWhileIdle(true);
		// try {
		// 	this.dataSource.setFilters("stat");
		// } catch (Exception e) {
		// 	e.printStackTrace();
		// }
	}

	public DruidDataSource getDataSource() {
		return dataSource;
	}

	public ImConfig getImConfig() {
		return imConfig;
	}

	public void setImConfig(ImConfig imConfig) {
		this.imConfig = imConfig;
	}
	
}
