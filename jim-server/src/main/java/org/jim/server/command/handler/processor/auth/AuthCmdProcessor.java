/**
 * 
 */
package org.jim.server.command.handler.processor.auth;

import org.tio.core.ChannelContext;
import org.jim.common.packets.RespBody;
import org.jim.server.command.handler.processor.CmdProcessor;

import com.alibaba.fastjson.JSONObject;

public interface AuthCmdProcessor extends CmdProcessor {
    
	/**
	 * 分发接口
	 * @param reqBody
	 * @param channelContext
	 * @return
	 */
	public RespBody doDistribute(JSONObject reqBody ,ChannelContext channelContext);

}
