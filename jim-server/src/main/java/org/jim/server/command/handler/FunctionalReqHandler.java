package org.jim.server.command.handler;

import com.alibaba.fastjson.JSONObject;

import org.jim.common.ImAio;
import org.jim.common.ImPacket;
import org.jim.common.message.MessageHelper;
import org.jim.common.packets.Command;
import org.jim.common.packets.RespBody;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.tio.core.ChannelContext;

/**
 *
 */
public class FunctionalReqHandler extends AbstractCmdHandler
{
	@Override
	public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception
	{
		String userId = channelContext.getUserid();
		RespBody respBody = new RespBody(Command.COMMAND_CANCEL_MSG_RESP);
		JSONObject reqBody = JsonKit.toBean(packet.getBody(), JSONObject.class);
		if(reqBody.getIntValue("type")==1){
			long now = System.currentTimeMillis();
			long timeStamp = reqBody.getLongValue("time");
			if(reqBody.get("target")!=null && now - timeStamp < 600*1000l){
				String fromUser = reqBody.getString("target");
				MessageHelper messageHelper = imConfig.getMessageHelper();
				int result = messageHelper.setLastRead(fromUser, userId, timeStamp);
				if(result>0){
					respBody.setCode(10023);
					respBody.setMsg("success");
					respBody.setData(new JSONObject(){
						private static final long serialVersionUID = 1L;{
							put("to", userId);
							put("from", fromUser);
							put("readTime", timeStamp);
						}
					});
					// 同时通知聊天双方
					ImPacket backPacket = ImKit.ConvertRespPacket(respBody,channelContext);
					ImAio.sendToUser(userId, backPacket);
					ImAio.sendToUser(fromUser, backPacket);
					return null;
				}
				else{
					respBody.setCode(10024);
					respBody.setMsg("failed");	
				}
			}
			else{
				respBody.setCode(10024);
				respBody.setMsg("Time Out!");
			}
		}
		else if(reqBody.getIntValue("type")==2){
			respBody.setCode(10025);
			respBody.setMsg("debug");
		}
		ImPacket backPacket = ImKit.ConvertRespPacket(respBody,channelContext);
		return backPacket;
	}

	@Override
	public Command command() {
		return Command.COMMAND_CANCEL_MSG_REQ;
	}
}
