package org.jim.server.command.handler;

import org.jim.common.ImPacket;
import org.jim.common.ImStatus;
import org.tio.core.ChannelContext;
import org.jim.common.packets.AuthReqBody;
import org.jim.common.packets.Command;
import org.jim.common.packets.RespBody;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;

import org.jim.server.command.handler.processor.auth.AuthCmdProcessor;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 版本: [1.0]
 * 功能说明: 鉴权请求消息命令处理器
 * @author : WChao 创建时间: 2017年9月13日 下午1:39:35
 */
public class AuthReqHandler extends AbstractCmdHandler
{
	@Override
	public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception
	{
		if (packet.getBody() == null) {
			RespBody respBody = new RespBody(Command.COMMAND_AUTH_RESP,ImStatus.C10010);
			return ImKit.ConvertRespPacket(respBody, channelContext);
		}

		List<AuthCmdProcessor> authProcessors = this.getProcessor(channelContext, AuthCmdProcessor.class);
		if(CollectionUtils.isEmpty(authProcessors)){
			AuthReqBody authReqBody = JsonKit.toBean(packet.getBody(), AuthReqBody.class);
			authReqBody.setToken( "Test");
			authReqBody.setCmd(null);
			RespBody respBody = new RespBody(Command.COMMAND_AUTH_RESP,ImStatus.C10009).setData(authReqBody);
			return ImKit.ConvertRespPacket(respBody, channelContext);
		}
		AuthCmdProcessor authServiceHandler = authProcessors.get(0);
		JSONObject authReqBody = JsonKit.toBean(packet.getBody(), JSONObject.class);
		RespBody respBody = authServiceHandler.doDistribute(authReqBody,channelContext);
		return ImKit.ConvertRespPacket(respBody, channelContext);
	}

	@Override
	public Command command() {
		return Command.COMMAND_AUTH_REQ;
	}
}
