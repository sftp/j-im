package org.jim.server.command.handler;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.packets.ChatBody;
import org.jim.common.packets.ChatType;
import org.jim.common.packets.Command;
import org.jim.common.packets.MsgType;
import org.jim.common.packets.RespBody;
import org.jim.common.utils.ChatKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.processor.chat.ChatCmdProcessor;
import org.jim.server.command.handler.processor.chat.MsgQueueRunnable;
import org.tio.core.ChannelContext;

import java.util.List;

import org.jim.common.ImStatus;
import org.jim.common.utils.ImKit;
import org.jim.common.packets.User;
import org.jim.common.message.MessageHelper;
import com.alibaba.fastjson.JSONObject;

/**
 * 版本: [1.0]
 * 功能说明: 聊天请求cmd消息命令处理器
 * @author : WChao 创建时间: 2017年9月22日 下午2:58:59
 */
public class ChatReqHandler extends AbstractCmdHandler {

	@Override
	public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
		if (packet.getBody() == null) {
			throw new Exception("body is null");
		}
		ChatBody chatBody = ChatKit.toChatBody(packet.getBody(), channelContext);
		packet.setBody(chatBody.toByte());
		//聊天数据格式不正确
		if(chatBody == null || chatBody.getChatType() == null){
			ImPacket respChatPacket = ChatKit.dataInCorrectRespPacket(channelContext);
			return respChatPacket;
		}
		// 修正发送时间
		Long fromTimeId = chatBody.getCreateTime(); //发送者传来的TimeId
		chatBody.setCreateTime(System.currentTimeMillis());

		// 机器人自动应答
		if("robot".equals(chatBody.getTo())){
			if(MsgType.MSG_TYPE_TEXT.getNumber() == chatBody.getMsgType()){ // 只回应文本消息
				List<ChatCmdProcessor> replyProcessors = this.getProcessor(channelContext, ChatCmdProcessor.class);
				if(CollectionUtils.isNotEmpty(replyProcessors)){
					ChatCmdProcessor chatServiceHandler = replyProcessors.get(0);
					RespBody respBody = chatServiceHandler.doAutoReply(packet,channelContext);
					if(respBody!=null){
						return ImKit.ConvertRespPacket(respBody, channelContext);
					}
				}
			}
			return null;
		}
		else if(ChatType.CHAT_TYPE_PRIVATE.getNumber() == chatBody.getChatType()){
			// 私聊 放入接收者信息
			MessageHelper messageHelper = imConfig.getMessageHelper();
			User ToUser = messageHelper.getUserByType(chatBody.getTo(), 2);
			if(ToUser == null) {	
				// 接收对象近期未上线 (用户info缓存保存7天，缓存失效即近期未上线，将阻止消息发送)
				RespBody chatDataInCorrectRespPacket = new RespBody(Command.COMMAND_CHAT_RESP,ImStatus.C10022);
				ImPacket respPacket = ImKit.ConvertRespPacket(chatDataInCorrectRespPacket, channelContext);
				respPacket.setStatus(ImStatus.C10022);
				return respPacket;
			}
			else{
				if(chatBody.getExtras()!=null ){
					chatBody.getExtras().put("ToUser", ToUser.getNick());
					chatBody.getExtras().put("ToAvatar", ToUser.getAvatar());
				}
				else {
					JSONObject extras = new JSONObject();
					extras.put("ToUser", ToUser.getNick());
					extras.put("ToAvatar", ToUser.getAvatar());
					chatBody.setExtras(extras);
				}
				packet.setBody(chatBody.toByte());
			}
		}

		List<ChatCmdProcessor> chatProcessors = this.getProcessorNotEqualName(Sets.newHashSet(ImConst.BASE_ASYNC_CHAT_MESSAGE_PROCESSOR),ChatCmdProcessor.class);
		if(CollectionUtils.isNotEmpty(chatProcessors)){
			chatProcessors.forEach(chatProcessor -> chatProcessor.handler(packet,channelContext));
		}
		//异步调用业务处理消息接口
		if(ChatType.forNumber(chatBody.getChatType()) != null){
			MsgQueueRunnable msgQueueRunnable = (MsgQueueRunnable)channelContext.getAttribute(ImConst.CHAT_QUEUE);
			msgQueueRunnable.addMsg(packet);
			msgQueueRunnable.getExecutor().execute(msgQueueRunnable);
		}
		ImPacket chatPacket = new ImPacket(Command.COMMAND_CHAT_REQ,new RespBody(Command.COMMAND_CHAT_REQ,chatBody).toByte());
		//设置同步序列号;
		chatPacket.setSynSeq(packet.getSynSeq());
		//私聊
		if(ChatType.CHAT_TYPE_PRIVATE.getNumber() == chatBody.getChatType()){
			String toId = chatBody.getTo();
			if(ChatKit.isOnline(toId,imConfig)){
				ImAio.sendToUser(toId, chatPacket);
				//发送成功响应包
				return ChatKit.sendSuccessRespPacket(channelContext,fromTimeId,chatBody.getId());
			}else{
				//用户不在线响应包
				return ChatKit.offlineRespPacket(channelContext,fromTimeId,chatBody.getId());
			}
			//群聊
		}else if(ChatType.CHAT_TYPE_PUBLIC.getNumber() == chatBody.getChatType()){
			String group_id = chatBody.getGroup_id();
			ImAio.sendToGroup(group_id, chatPacket);
			//发送成功响应包
			return ChatKit.sendSuccessRespPacket(channelContext);
		}
		return null;
	}
	@Override
	public Command command() {
		return Command.COMMAND_CHAT_REQ;
	}
}
