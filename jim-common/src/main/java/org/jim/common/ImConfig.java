/**
 * 
 */
package org.jim.common;

import org.jim.common.config.Config;
import org.jim.common.http.HttpConfig;
import org.jim.common.ws.WsServerConfig;
import org.jim.common.message.DBUtilsHelper;
/**
 * @author WChao
 *
 */
public class ImConfig extends Config{
	
	/**
	 * http相关配置;
	 */
	private HttpConfig httpConfig;
	/**
	 * WebSocket相关配置;
	 */
	private WsServerConfig wsServerConfig;

	/**
	 * sql相关配置;
	 */
	private String sqlUrl = "jdbc:mysql://localhost:3306/test?characterEncoding=utf8";
	private String sqlUser = "root";
	private String sqlPassword = "root";
	private DBUtilsHelper dbUtilsHelper;

	/**
	 * 微服务网关地址
	 */
	private String apiGateWay = "http://127.0.0.1:9200";
	public String getApiGateWay() {
		return apiGateWay;
	}
	public void setApiGateWay(String apiGateWay) {
		this.apiGateWay = apiGateWay;
	}

	public ImConfig() {
		this.httpConfig = new HttpConfig();
		this.wsServerConfig = new WsServerConfig();
	}
	
	public ImConfig(String bindIp,Integer bindPort){
		this.bindIp = bindIp;
		this.bindPort = bindPort;
		this.httpConfig = new HttpConfig();
		this.wsServerConfig = new WsServerConfig();
	}

	public HttpConfig getHttpConfig() {
		return httpConfig;
	}

	public void setHttpConfig(HttpConfig httpConfig) {
		this.httpConfig = httpConfig;
	}

	public WsServerConfig getWsServerConfig() {
		return wsServerConfig;
	}

	public void setWsServerConfig(WsServerConfig wsServerConfig) {
		this.wsServerConfig = wsServerConfig;
	}

	public DBUtilsHelper getDBUtils() {
		return dbUtilsHelper;
	}

	public void setDBUtils(DBUtilsHelper dbUtilsHelper) {
		this.dbUtilsHelper = dbUtilsHelper;
	}

	public String getSqlUrl() {
		return this.sqlUrl;
	}
	public String getSqlUser() {
		return this.sqlUser;
	}
	public String getSqlPassword() {
		return this.sqlPassword;
	}
	public void setSqlUrl(String sqlUrl) {
		this.sqlUrl = sqlUrl;
	}
	public ImConfig setSqlUser(String sqlUser) {
		this.sqlUser = sqlUser;
		return this;
	}
	public ImConfig setSqlPassword(String sqlPassword) {
		this.sqlPassword = sqlPassword;
		return this;
	}

}
