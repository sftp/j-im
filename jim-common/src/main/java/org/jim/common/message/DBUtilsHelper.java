package org.jim.common.message;

import javax.sql.DataSource;

public interface DBUtilsHelper {

    public DataSource getDataSource();
    
    
}
